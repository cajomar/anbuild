# Anbuild #
## What ##
A tool to build cmake-based projects into an Android APK.  

## Why ##
I wanted a way to write cross-platform games (preferably in C; I'm too lazy to
learn a new language like [Dart](https://dart.dev/).) I had been using the
[Android NDK](https://developer.android.com/ndk/) with a bit of Java glue, but
with only 8GB of RAM and no SSD, running android studio was a painful
experience. Then I found
[rawdrawandroid](https://github.com/cnlohr/rawdrawandroid), which did exactly 
what I wanted. Amost. I wanted a tool that could be used with minimal changes
for multiple projects, and I wanted to use CMake for the actuall building.  
So I created this. First as a Bash script, then a Makefile, but now as a
[Taskfile.yaml](https://github.com/go-task/task), because I wanted something
that creates a parallel dependency tree, but revolves less around generating
individual files (CMake does that).

## How ##
You need a copy of [task](https://github.com/go-task/task). See their
[installation instructions](https://taskfile.dev/#/installation).
Depending on how you installed it, you may need to change the shebang line
to wherever the `task` executable is installed:
```console
$ sed "1s|.*|$(which task) --taskfile|" -i bin/anbuild
```
You also need the command line tools of the Android SDK and the Android NDK.  
Then you can execute
[bin/anbuild](https://bitbucket.org/cajomar/anbuild/src/master/bin/anbuild)
in a directory containing a `CMakeLists.txt`.  

## When ##
I still need to do a lot more work on this, like adding the ability to set your
own values per project in an `anbuild_config.sh`. If I can't get task to do
that I might have to forfiet the parallellization and go back to Bash.
