# Anbuild
## What
A tool to build cmake-based projects into an Android APK **without** Android
studio.

## Why
I wanted a way to write cross-platform games (preferably in C; I'm too lazy to
learn a new language like [Dart](https://dart.dev/)).
Using the [Android NDK](https://developer.android.com/ndk/) in Android Studio
lets you make C/C++ modules in a Java/Kotlin application, but with only 8GB of
RAM and no SSD, running android studio was a painful experience. Then I found
[rawdrawandroid](https://github.com/cnlohr/rawdrawandroid), which did almost 
what I wanted: compile a C game into an Android APK.  
This was good, but I wanted a tool that could be used with minimal changes
for multiple projects, and something that wouldn't require many changes in the
project's codebase. I also wanted to use CMake for the build system.  
So I created this. First as a Bash script, then a Makefile, but now as a
[Taskfile.yaml](https://github.com/go-task/task), because I wanted something
that creates a parallel dependency tree, but revolves less around generating
individual files (that's CMake's job).

## How
You need a copy of [task](https://github.com/go-task/task). See their
[installation instructions](https://taskfile.dev/#/installation).
Depending on how you installed it, you may need to change the shebang line
to wherever the `task` executable is installed:
```console
$ sed "1s|.*|$(which task) --taskfile|" -i bin/anbuild
```
You also need the command line tools of the Android SDK and the Android NDK.  
Then you can execute
[bin/anbuild](https://gitlab.com/cajomar/anbuild/-/blob/master/bin/anbuild)
from a directory containing a `CMakeLists.txt`.  
Instead of creating an executable you will need to create a shared library:
```cmake
if (ANDROID)
    add_library(myproject SHARED main.c)
else()
    add_executable(myproject main.c)
endif()
```
You will also need to handle window creation. `android_native_app_glue.c` is a
good start. I also recommend looking at the
[NDK samples](https://github.com/android/ndk-samples/).
Unfortunatly at the time of writing this, [SDL2](https://libsdl.org) seems to
only work with Gradle. If sometime in the future it is possible to compile SDL
as a native activity please let me know.

## When
I still need to do a lot more work on this, like adding the ability to set your
own values per project in an `anbuild_config.sh`. If I can't get task to do
that I might have to forfiet the parallellization and go back to Bash.
